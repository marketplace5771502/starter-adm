package ru.pet.shop.vlad.fateev.starteradm.dto;

import lombok.Data;

@Data
public class AdmDto {
    private String setting;
    private String value;
}
