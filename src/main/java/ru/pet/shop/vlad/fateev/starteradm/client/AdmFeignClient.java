package ru.pet.shop.vlad.fateev.starteradm.client;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ru.pet.shop.vlad.fateev.starteradm.dto.AdmDto;
import ru.pet.shop.vlad.fateev.starteradm.setting.AdmSetting;

import java.util.List;

@FeignClient("ADMINISTRATIVE-SETUP-SERVICE")
@EnableFeignClients
public interface AdmFeignClient {

    @GetMapping("/adm/setting/by_id/{setting_id}")
    AdmDto getSettingById(@PathVariable("setting_id") Long id);

    @GetMapping("/adm/setting/by_enum/{setting}")
    AdmDto getSettingByString(@PathVariable("setting") AdmSetting setting);

    @GetMapping("/adm/settings/by_enum/{settings}")
    List<AdmDto> getListSetting(@PathVariable("settings") List<AdmSetting> settings);
}

