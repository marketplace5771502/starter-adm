package ru.pet.shop.vlad.fateev.starteradm.setting;

public enum AdmSetting {

    PASSWORD_LIFE_TIME,
    TOKEN_LIFE_TIME,
    NUMBER_OF_CHARACTERS_IN_PASSWORD,
    ENABLE_TWO_FACTOR_AUTHENTICATION
}
